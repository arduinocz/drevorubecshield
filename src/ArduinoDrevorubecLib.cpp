/*
 Name:		ArduinoDrevorubecLib.cpp
 Created:	10/11/2016 8:31:45 PM
 Author:	Mates
 Editor:	http://www.visualmicro.com
*/

#include "ArduinoDrevorubecLib.h"

Servo Servo1;
Servo Servo2;

//inicializace shiledu
DrevorubecShield::DrevorubecShield()
{
		_servo1_notouch_pozice = NOTOUCHPOS;
		_servo2_notouch_pozice = NOTOUCHPOS;
		_servo1_touch_pozice = TOUCHPOS;
		_servo2_touch_pozice = TOUCHPOS;
		pinMode(BUTTON1_PIN, INPUT);
		pinMode(BUTTON2_PIN, INPUT);
		pinMode(SERVO1_PIN, OUTPUT);
		pinMode(SERVO2_PIN, OUTPUT);
}

//pro spr�vnou aktivaci shiledu mus� b�t ve funkci setup() zavol�na tato funkce stejn� jako "Serial.begin()"
void DrevorubecShield::begin()
{
	/*p�ipojen� serv*/
	Servo1.attach(SERVO1_PIN);
	Servo2.attach(SERVO2_PIN);

	/*nasledne nastaveni na serv do defaultn� pozice*/
	Servo1.write(_servo1_notouch_pozice);
	Servo2.write(_servo2_notouch_pozice);

	/*inicializace senzoru mod1024 viz example v knihovne MOD1024.h*/
	Wire.begin();
	mod1024.init();
	mod1024.VEML6040_setIT(MERENI);
	mod1024.VEML6040_autoMode();
	mod1024.VEML6040_enableSensor();
}

//a p��ladn� se pak shield m��e i deaktivovat t�mto p��kazem
void DrevorubecShield::end()
{
	Servo1.detach();
	Servo2.detach();

	Wire.end();

}

//upravuje z�kladn� pozice serva - pozice p�i dotyku, pozice p�i nedotyku jedn�m argumentem 'odchylka'
//kladn� odchylka obrac� servo od displaye, z�porn� pak k displayi
void DrevorubecShield::NastavServo1(int odchylka)
{
	_servo1_notouch_pozice = NOTOUCHPOS + odchylka;
	_servo1_touch_pozice = TOUCHPOS + odchylka;
}

void DrevorubecShield::NastavServo2(int odchylka)
{
	_servo2_notouch_pozice = NOTOUCHPOS + odchylka;
	_servo2_touch_pozice = TOUCHPOS + odchylka;
}

//p�i zad�n� 2 ardument� ten prvn� nastav� odchylku p�i dotyku, druh� pak nastav� odchylku p�i nedotyku
void DrevorubecShield::NastavServo1(int odchylkaTouch, int odchylkaNoTouch)
{
	_servo1_notouch_pozice = NOTOUCHPOS + odchylkaNoTouch;
	_servo1_touch_pozice = TOUCHPOS + odchylkaTouch;
}

void DrevorubecShield::NastavServo2(int odchylkaTouch, int odchylkaNoTouch)
{
	_servo2_notouch_pozice = NOTOUCHPOS + odchylkaNoTouch;
	_servo2_touch_pozice = TOUCHPOS + odchylkaTouch;
}

//jednoduch� funkce, kter� vrac� jestli je, nebo nen� ttla��tko stiskl� v dan� moment
boolean DrevorubecShield::jeTlacitko1Stiskle()
{
	return !(PIND & B00000100);  //p��m� �ten� portu - to same jako !digitalRead(2);  ale rychlej�� 	
}

boolean DrevorubecShield::jeTlacitko2Stiskle()
{
	return !(PIND & B00001000);  //p��m� �ten� portu - to same jako !digitalRead(3);  ale rychlej�� 	
}

//argumenty: true, false - uvede servo do pozice dotyku nebo nedotyku
void DrevorubecShield::servo1Dotyk(boolean stisk)
{
	jeServo1Dotyk = stisk;
	if (jeServo1Dotyk) {
		Servo1.write(_servo1_touch_pozice);
	}
	else {
		Servo1.write(_servo1_notouch_pozice);
	}
}

void DrevorubecShield::servo2Dotyk(boolean stisk)
{
	jeServo2Dotyk = stisk;
	if (jeServo2Dotyk) {
		Servo2.write(_servo2_touch_pozice);
	}
	else {
		Servo2.write(_servo2_notouch_pozice);
	}
}

//tyto funkce vrac� aktu�ln� barvu m��enou senzorem

//b�la barva se vrac� ve funkcu getRed() z knihovny.. zvl�tn� �e?
unsigned int DrevorubecShield::vratBilo()
{
	return mod1024.getRed();
}

//atd..
unsigned int DrevorubecShield::vratCerv()
{
	return mod1024.getGreen();
}

unsigned int DrevorubecShield::vratZele()
{
	return mod1024.getBlue();
}

unsigned int DrevorubecShield::vratModr()
{
	return mod1024.getWhite();
}







//=================================DREVORUBECSHIELDSTATUS=========================================//







void DrevorubecShieldStatus::update()
{
	senzor.bila = vratBilo();
	senzor.cerv = vratCerv();
	senzor.modr = vratModr();
	senzor.zele = vratZele();

	tlac1Stiskle = jeTlacitko1Stiskle();
	tlac2Stiskle = jeTlacitko2Stiskle();

	if (tlac1Stiskle == true && _previousShieldState.tlac1Stiskle == false) {
		tlac1Stisknuto();
		_previousShieldState.tlac1Stiskle = tlac1Stiskle;
	}
	else if (tlac1Stiskle == false && _previousShieldState.tlac1Stiskle == true) {
		tlac1Pusteno();
		_previousShieldState.tlac1Stiskle = tlac1Stiskle;
	}

	if (tlac2Stiskle == true && _previousShieldState.tlac2Stiskle == false) {
		tlac2Stisknuto();
		_previousShieldState.tlac2Stiskle = tlac2Stiskle;
	}
	else if (tlac2Stiskle == false && _previousShieldState.tlac2Stiskle == true) {
		tlac2Pusteno();
		_previousShieldState.tlac2Stiskle = tlac2Stiskle;
	}


}



