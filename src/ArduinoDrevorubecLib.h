/*
 Name:		ArduinoDrevorubecLib.h
 Created:	10/11/2016 8:31:45 PM
 Author:	Mates
 Editor:	http://www.visualmicro.com
*/

#ifndef _ArduinoDrevorubecLib_h
#define _ArduinoDrevorubecLib_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include <MOD1024.h>
#include <Wire.h>
#include <Servo.h>

//tyto konstanty pou��v� knihovna MOD1024 pro nastaven� rychlosti m��en� pro RGBW senzor

#define MERENI_40ms 1
//#define MERENI_80ms 1
//#define MERENI_160ms 1
//#define MERENI_320ms 1
//#define MERENI_640ms 1
//#define MERENI_1280ms 1

//definice pin� shieldu
#define BUTTON1_PIN 2
#define BUTTON2_PIN 3
#define SERVO1_PIN 4
#define SERVO2_PIN 5

#ifdef MERENI_40ms
#define MERENI 0
#elif MERENI_80ms
#define MERENI 1
#elif MERENI_160ms
#define MERENI 2
#elif MERENI_320ms
#define MERENI 3
#elif MERENI_640ms
#define MERENI 4
#elif MERENI_1280ms
#define MERENI 5
#else 
#define MERENI 0
#endif // MERENI

//defaultn� poloha pro serva ve stupn�ch 0 - 180, d� se zm�nit funkc� NastavServoX();
#define NOTOUCHPOS 102
#define TOUCHPOS 90

#define SERVO1 1
#define SERVO2 2

//deklarace typu prom�nn� Color_T
struct Color_T
{
	//upln� by sta�il unsined int, ale pak by to mohlo n�koho m�st p�i ode��t�n� ��sel od sebe :)
	long cerv;       
	long zele;
	long modr;
	long bila;
};
struct ShieldState_T
{
	boolean tlac1Stiskle;
	boolean tlac2Stiskle;
};

/*ShieldState_T DrevorubecStatus;*/

class DrevorubecShield
{
public:
//V�echny z�kladn� funkce knihovny
	DrevorubecShield();
	void begin();
	void end();
	void NastavServo1(int odchylka);
	void NastavServo2(int odchylka);
	void NastavServo1(int odchylkaTouch, int odchylkaNoTouch);
	void NastavServo2(int odchylkaTouch, int odchylkaNoTouch);
	unsigned int vratCerv();
	unsigned int vratZele();
	unsigned int vratModr();
	unsigned int vratBilo();
	boolean jeTlacitko1Stiskle();
	boolean jeTlacitko2Stiskle();
	boolean jeServo1Dotyk;
	boolean jeServo2Dotyk;
	void servo1Dotyk(boolean stisk);
	void servo2Dotyk(boolean stisk);
private:
	byte _servo1_notouch_pozice;
	byte _servo2_notouch_pozice;
	byte _servo1_touch_pozice;
	byte _servo2_touch_pozice;
};

class DrevorubecShieldStatus : public DrevorubecShield
{
public:
//Pro jin� p��stup k �e�en� programu jsem vytvo�il i t��du
//DrevorubecShieldStatus, kter� je v�ce rozveden� v p��kladu
	void update();
	Color_T senzor;
	boolean tlac1Stiskle;
	boolean tlac2Stiskle;
	virtual void tlac1Stisknuto();
	virtual void tlac2Stisknuto();
	virtual void tlac1Pusteno();
	virtual void tlac2Pusteno();
private:
	ShieldState_T _previousShieldState;
};

extern DrevorubecShield Drevorubec;
//extern DrevorubecShieldStatus DrevorubecStatus;


#endif

