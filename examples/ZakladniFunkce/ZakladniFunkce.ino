#include <ArduinoDrevorubecLib.h>

DrevorubecShield Drevorubec;

void setup()
{
  Serial.begin(9600); 
  //přiblízení serva blíž k displayi o 10 stupnu
  Drevorubec.NastavServo1(-10);
  //oddalení serva od displaye v pozici 'TOUCH' o 3 stupně k displayi a v pozici 'NOTOUCH' o 5 stupnu od displaye
  Drevorubec.NastavServo2(-3, 5);
  //lepsí aktivovat shield az po uvedeni odchylek
  Drevorubec.begin();
}

void loop()
{
  unsigned int cervenaZeSenzoru = Drevorubec.vratCerv();
  unsigned int zelenaZeSenzoru = Drevorubec.vratZele();
  unsigned int modraZeSenzoru = Drevorubec.vratModr();
  unsigned int bilaZeSenzoru = Drevorubec.vratBilo();

  Serial.print("Senzor R,G,B,W: ");
  Serial.print(cervenaZeSenzoru);
  Serial.print(", ");
  Serial.print(zelenaZeSenzoru);
  Serial.print(", ");
  Serial.print(modraZeSenzoru);
  Serial.print(", ");
  Serial.println(bilaZeSenzoru);

  if (Drevorubec.jeTlacitko1Stiskle()) {
    Serial.println("tlačítko 1 je stiskle!");
    Drevorubec.servo1Dotyk(true);
  }
  else {
    Drevorubec.servo1Dotyk(false);
  }

  if (Drevorubec.jeTlacitko2Stiskle()) {
    Drevorubec.servo2Dotyk(true);
  }
  else {
    Drevorubec.servo2Dotyk(false);
  }

  delay(10);
}


