
/*
  Name:		ArduinoDrevorubec.ino
  Created:	10/11/2016 8:31:45 PM
  Author:	Matej Suchanek www.arduino.cz
  Editor:	http://www.visualmicro.com
*/

/*
ShieldStatus je WIP koncept, který jsem si vymyslel, (teda, asi je už dávno vymyslený, ale ještě jsem na nic takového nenarazil)
založený na pravidelných updatech statusu shieldu, 
nejlépe vnitřním časovačem arduina TIMEREM 2 (zatím ale nepropojeným), kde nám tato třída 
umožňuje jiný úhel pohledu na tvorbu programu mírně se podobajícímu základům 
objektovému programování např C#, co nas momentalne uci ve skole :)
*/



#include <ArduinoDrevorubecLib.h>

DrevorubecShield Drevorubec;
DrevorubecShieldStatus DrevorubecStatus;

void setup()
{
  Serial.begin(9600);
  pinMode(13, OUTPUT);

  int odchylka1 = -12; //o 12 stupnu natocit servo1 k displayi
  int odchylka2Doteku = -15; //o 15 stupnu natocit servo2 k displayi pri doteku (TOUCH)
  int odchylka2DeDoteku = 3; //o 3 stupne natocit servo2 od displaye pri pusteni (NOTOUCH)
  Drevorubec.NastavServo1(odchylka1); //NastavServoX(int odchylka);
  Drevorubec.NastavServo2(odchylka2Doteku, odchylka2DeDoteku);  //NastavServoX(int odchylkaTouch, int odchylkaNoTouch);
  Drevorubec.begin();	//inicializace Shieldu

}

unsigned long posledniMillis;
const int msUpdate = 40;

void loop()
{
  if (millis() - posledniMillis > msUpdate) {  //kaydzch 40ms..
    posledniMillis = millis();
    DrevorubecStatus.update();               //..dojke k updatu statusu shieldu

    //následnující kod se vykonná každých 40ms - podle nejvyssi rychlosti čtení RGBW senzoru

    //hodnoty se pak projevují v proměnných a nepřistupujeme k nim tedy přes funkce
    boolean b1 = DrevorubecStatus.jeServo1Dotyk;
    boolean b2 = DrevorubecStatus.tlac2Stiskle;

    //ano, promenne ve strukture senzor jsou typu long
    long cervenaZeSenzoru = DrevorubecStatus.senzor.cerv;
    long zelenaZeSenzoru = DrevorubecStatus.senzor.zele;
    long modraZeSenzoru = DrevorubecStatus.senzor.modr;
    long bilaZeSenzoru = DrevorubecStatus.senzor.bila;

    Serial.print("Senzor R,G,B,W: ");
    Serial.print(cervenaZeSenzoru);
    Serial.print(", ");
    Serial.print(zelenaZeSenzoru);
    Serial.print(", ");
    Serial.print(modraZeSenzoru);
    Serial.print(", ");
    Serial.println(bilaZeSenzoru);
  }


}


//update pote vyvolava tyto funkce

void DrevorubecShieldStatus::tlac1Stisknuto() {
  Drevorubec.servo1Dotyk(true);
}

void DrevorubecShieldStatus::tlac1Pusteno() {
  Drevorubec.servo1Dotyk(false);
}

void DrevorubecShieldStatus::tlac2Stisknuto() {
  digitalWrite(13, HIGH);
}

void DrevorubecShieldStatus::tlac2Pusteno() {
  digitalWrite(13, LOW);
}

//vice informaci v .h souboru :)


